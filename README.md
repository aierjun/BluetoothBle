#BluetoothBle
使用blelib库对ble蓝牙设备进行连接的example，全部在Application中运行，通过activity的广播启动蓝牙连接，断开后会自动重新连接。已写好发送和接受数据的方法，接收数据后会发送广播。

BleLib github：https://github.com/junkchen/BleLib.git